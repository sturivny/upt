"""Examples for Restraint XML Elements."""
# pylint: disable=invalid-name

fetch_example = """
<fetch url="https://github.com/beaker-project/beaker-core-tasks/archive/master.zip#check-install"/>
"""

log_example = """
<log path="recipes/11934298/tasks/1/logs/harness.log" filename="harness.log"/>
"""

param_example = """
<param name="CKI_NAME" value="SELinux Custom Module Setup"/>
"""

result_example = """
<result id="1" path="/distribution/check-install" result="PASS">TEXT GOES HERE
  <logs>
    <log path="recipes/1/tasks/1/results/1/logs/dmesg.log" filename="dmesg.log"/>
    <log path="recipes/1/tasks/1/results/1/logs/avc.log" filename="avc.log"/>
  </logs>
</result>
"""

task_example = """
<task name="Boot test" role="STANDALONE" id="1" status="Completed" result="PASS" start_time="2022-05-06T20:26:58+0000" end_time="2022-05-06T20:27:56+0000" duration="58">
  <logs>
    <log path="recipes/1/tasks/1/logs/harness.log" filename="harness.log"/>
    <log path="recipes/1/tasks/1/logs/taskout.log" filename="taskout.log"/>
    <log path="recipes/1/tasks/1/logs/io_perf_base_kernel.log" filename="io_perf_base_kernel.log"/>
    <log path="recipes/1/tasks/1/logs/kernel_config.log" filename="kernel_config.log"/>
    <log path="recipes/1/tasks/1/logs/io_perf_cki_kernel.log" filename="io_perf_cki_kernel.log"/>
    <log path="recipes/1/tasks/1/logs/test_console.log" filename="test_console.log"/>
  </logs>
  <fetch url="https://server/kernel-tests-main.zip#distribution/kpkginstall"/>
  <params>
    <param name="CKI_ID" value="3"/>
    <param name="CKI_NAME" value="Boot test"/>
    <param name="CKI_UNIVERSAL_ID" value="boot"/>
    <param name="CKI_MAINTAINERS" value="Bruno Goncalves &lt;bgoncalv@redhat.com&gt; / bgoncalv, Jeff Bastian &lt;jbastian@redhat.com&gt; / jbastianrh"/>
    <param name="KPKG_URL" value="https://server/arch#package_name=kernel"/>
    <param name="KILLTIMEOVERRIDE" value="3600"/>
    <param name="STANDALONE" value="host-abc.redhat.com"/>
    <param name="JOB_MEMBERS" value="host-abc.redhat.com"/>
    <param name="RECIPE_MEMBERS" value="host-abc.redhat.com"/>
  </params>
  <results>
    <result id="1" path="distribution/kpkginstall/kernel-in-place" result="PASS">
      <logs>
        <log path="recipes/1/tasks/4/results/1/logs/dmesg.log" filename="dmesg.log"/>
        <log path="recipes/1/tasks/4/results/1/logs/avc.log" filename="avc.log"/>
      </logs>
    </result>
    <result id="2" path="distribution/kpkginstall/reboot" result="PASS">
      <logs>
        <log path="recipes/1/tasks/4/results/2/logs/dmesg.log" filename="dmesg.log"/>
        <log path="recipes/1/tasks/4/results/2/logs/avc.log" filename="avc.log"/>
      </logs>
    </result>
  </results>
</task>
"""

recipe_example = """
<recipe id="1" status="Completed" result="WARN" job_id="1" system="foo@bar.com">
  <task name="SELinux Custom Module Setup" role="None" id="1" status="Completed" result="None" start_time="2022-05-06T20:14:07+0000" end_time="2022-05-06T20:14:23+0000" duration="16">
    <logs>
      <log path="recipes/1/tasks/1/logs/harness.log" filename="harness.log"/>
      <log path="recipes/1/tasks/1/logs/taskout.log" filename="taskout.log"/>
    </logs>
    <fetch url="https://server/kernel-tests-main.zip#distribution/selinux-custom-modules"/>
    <params>
      <param name="CKI_ID" value="1"/>
      <param name="CKI_NAME" value="SELinux Custom Module Setup"/>
      <param name="CKI_UNIVERSAL_ID" value="selinux_custom_module_setup"/>
      <param name="CKI_WAIVED" value="True"/>
      <param name="None" value="host-abc.redhat.com"/>
      <param name="JOB_MEMBERS" value="host-abc.redhat.com"/>
      <param name="RECIPE_MEMBERS" value="host-abc.redhat.com"/>
    </params>
    <results>
      <result id="1" path="/kernel/distribution/selinux-custom-modules" result="SKIP">
        <logs>
          <log path="recipes/1/tasks/1/results/1/logs/dmesg.log" filename="dmesg.log"/>
        </logs>
      </result>
    </results>
  </task>
  <task name="/distribution/check-install" role="STANDALONE" id="2" status="Completed" result="PASS" start_time="2022-05-06T20:14:23+0000" end_time="2022-05-06T20:14:41+0000" duration="18">
    <logs>
      <log path="recipes/1/tasks/2/logs/harness.log" filename="harness.log"/>
      <log path="recipes/1/tasks/2/logs/taskout.log" filename="taskout.log"/>
    </logs>
    <fetch url="https://git-server/archive/master.zip#check-install"/>
    <params>
      <param name="STANDALONE" value="host-abc.redhat.com"/>
      <param name="JOB_MEMBERS" value="host-abc.redhat.com"/>
      <param name="RECIPE_MEMBERS" value="host-abc.redhat.com"/>
    </params>
    <results>
      <result id="1" path="/distribution/check-install" result="PASS">
        <logs>
          <log path="recipes/1/tasks/2/results/1/logs/dmesg.log" filename="dmesg.log"/>
          <log path="recipes/1/tasks/2/results/1/logs/avc.log" filename="avc.log"/>
        </logs>
      </result>
      <result id="2" path="/distribution/check-install/Sysinfo" result="PASS">
        <logs>
          <log path="recipes/1/tasks/2/results/2/logs/avc.log" filename="avc.log"/>
        </logs>
      </result>
    </results>
  </task>
</recipe>
"""

recipeset_example = """
<recipeSet id="1">
  <recipe id="1" status="Completed" result="PASS" job_id="1">
    <task name="SELinux Custom Module Setup" role="None" id="1" status="Completed" result="None" start_time="2022-05-17T14:55:40+0000" end_time="2022-05-17T14:56:02+0000" duration="22">
      <logs>
        <log path="recipes/1/tasks/1/logs/harness.log" filename="harness.log"/>
        <log path="recipes/1/tasks/1/logs/taskout.log" filename="taskout.log"/>
      </logs>
      <fetch url="https://server/kernel-tests-main.zip#distribution/selinux-custom-modules"/>
      <params>
        <param name="CKI_ID" value="33"/>
        <param name="CKI_NAME" value="SELinux Custom Module Setup"/>
        <param name="CKI_UNIVERSAL_ID" value="selinux_custom_module_setup"/>
        <param name="CKI_WAIVED" value="True"/>
        <param name="None" value="host-abc.redhat.com"/>
        <param name="JOB_MEMBERS" value="host-abc.redhat.com"/>
        <param name="RECIPE_MEMBERS" value="host-abc.redhat.com"/>
      </params>
      <results>
        <result id="1" path="/kernel/distribution/selinux-custom-modules" result="SKIP">
          <logs>
            <log path="recipes/1/tasks/1/results/1/logs/dmesg.log" filename="dmesg.log"/>
          </logs>
        </result>
      </results>
    </task>
    <task name="/distribution/check-install" role="STANDALONE" id="2" status="Completed" result="PASS" start_time="2022-05-17T14:56:03+0000" end_time="2022-05-17T14:56:21+0000" duration="18">
      <logs>
        <log path="recipes/1/tasks/2/logs/harness.log" filename="harness.log"/>
        <log path="recipes/1/tasks/2/logs/taskout.log" filename="taskout.log"/>
        <log path="recipes/1/tasks/2/logs/test_console.log" filename="test_console.log"/>
      </logs>
      <fetch url="https://git-server/archive/master.zip#check-install"/>
      <params>
        <param name="STANDALONE" value="host-abc.redhat.com"/>
        <param name="JOB_MEMBERS" value="host-abc.redhat.com"/>
        <param name="RECIPE_MEMBERS" value="host-abc.redhat.com"/>
      </params>
      <results>
        <result id="1" path="/distribution/check-install" result="PASS">
          <logs>
            <log path="recipes/1/tasks/2/results/1/logs/dmesg.log" filename="dmesg.log"/>
            <log path="recipes/1/tasks/2/results/1/logs/avc.log" filename="avc.log"/>
          </logs>
        </result>
        <result id="2" path="/distribution/check-install/Sysinfo" result="PASS">
          <logs>
            <log path="recipes/1/tasks/2/results/2/logs/avc.log" filename="avc.log"/>
          </logs>
        </result>
      </results>
    </task>
    <task name="/test/misc/machineinfo" id="3" status="Completed" result="PASS" start_time="2022-05-17T14:56:24+0000" end_time="2022-05-17T14:56:44+0000" duration="20">
      <logs>
        <log path="recipes/1/tasks/3/logs/harness.log" filename="harness.log"/>
        <log path="recipes/1/tasks/3/logs/taskout.log" filename="taskout.log"/>
        <log path="recipes/1/tasks/3/logs/machinedesc.log" filename="machinedesc.log"/>
        <log path="recipes/1/tasks/3/logs/lshw.log" filename="lshw.log"/>
        <log path="recipes/1/tasks/3/logs/df.log" filename="df.log"/>
        <log path="recipes/1/tasks/3/logs/mount.log" filename="mount.log"/>
        <log path="recipes/1/tasks/3/logs/installedpkgs.log" filename="installedpkgs.log"/>
        <log path="recipes/1/tasks/3/logs/test_console.log" filename="test_console.log"/>
      </logs>
      <fetch url="https://server/kernel-tests-main.zip#test/misc/machineinfo"/>
      <params>
        <param name="CKI_ID" value="34"/>
        <param name="CKI_NAME" value="machineinfo"/>
        <param name="CKI_UNIVERSAL_ID" value="machineinfo"/>
        <param name="CKI_WAIVED" value="True"/>
        <param name="JOB_MEMBERS" value="host-abc.redhat.com"/>
        <param name="RECIPE_MEMBERS" value="host-abc.redhat.com"/>
      </params>
      <results>
        <result id="1" path="test/misc/machineinfo" result="PASS">
          <logs>
            <log path="recipes/1/tasks/3/results/1/logs/dmesg.log" filename="dmesg.log"/>
            <log path="recipes/1/tasks/3/results/1/logs/avc.log" filename="avc.log"/>
          </logs>
        </result>
      </results>
    </task>
  </recipe>
  <recipe id="2" status="Completed" result="FAIL" job_id="1">
    <task name="SELinux Custom Module Setup" role="None" id="1" status="Completed" result="None" start_time="2022-05-17T14:55:38+0000" end_time="2022-05-17T14:55:55+0000" duration="17">
      <logs>
        <log path="recipes/2/tasks/1/logs/harness.log" filename="harness.log"/>
        <log path="recipes/2/tasks/1/logs/taskout.log" filename="taskout.log"/>
        <log path="recipes/2/tasks/1/logs/test_console.log" filename="test_console.log"/>
      </logs>
      <fetch url="https://server/kernel-tests-main.zip#distribution/selinux-custom-modules"/>
      <params>
        <param name="CKI_ID" value="39"/>
        <param name="CKI_NAME" value="SELinux Custom Module Setup"/>
        <param name="CKI_UNIVERSAL_ID" value="selinux_custom_module_setup"/>
        <param name="CKI_WAIVED" value="True"/>
        <param name="None" value="host-abc.redhat.com"/>
        <param name="JOB_MEMBERS" value="host-abc.redhat.com"/>
        <param name="RECIPE_MEMBERS" value="host-abc.redhat.com"/>
      </params>
      <results>
        <result id="1" path="/kernel/distribution/selinux-custom-modules" result="SKIP">
          <logs>
            <log path="recipes/2/tasks/1/results/1/logs/dmesg.log" filename="dmesg.log"/>
          </logs>
        </result>
      </results>
    </task>
    <task name="/distribution/check-install" role="STANDALONE" id="2" status="Completed" result="PASS" start_time="2022-05-17T14:55:56+0000" end_time="2022-05-17T14:56:13+0000" duration="17">
      <logs>
        <log path="recipes/2/tasks/2/logs/harness.log" filename="harness.log"/>
        <log path="recipes/2/tasks/2/logs/taskout.log" filename="taskout.log"/>
        <log path="recipes/2/tasks/2/logs/test_console.log" filename="test_console.log"/>
      </logs>
      <fetch url="https://git-server/archive/master.zip#check-install"/>
      <params>
        <param name="STANDALONE" value="host-abc.redhat.com"/>
        <param name="JOB_MEMBERS" value="host-abc.redhat.com"/>
        <param name="RECIPE_MEMBERS" value="host-abc.redhat.com"/>
      </params>
      <results>
        <result id="1" path="/distribution/check-install" result="PASS">
          <logs>
            <log path="recipes/2/tasks/2/results/1/logs/dmesg.log" filename="dmesg.log"/>
            <log path="recipes/2/tasks/2/results/1/logs/avc.log" filename="avc.log"/>
          </logs>
        </result>
        <result id="2" path="/distribution/check-install/Sysinfo" result="PASS">
          <logs>
            <log path="recipes/2/tasks/2/results/2/logs/avc.log" filename="avc.log"/>
          </logs>
        </result>
      </results>
    </task>
    <task name="/test/misc/machineinfo" id="3" status="Completed" result="PASS" start_time="2022-05-17T14:56:17+0000" end_time="2022-05-17T14:56:36+0000" duration="19">
      <logs>
        <log path="recipes/2/tasks/3/logs/harness.log" filename="harness.log"/>
        <log path="recipes/2/tasks/3/logs/taskout.log" filename="taskout.log"/>
        <log path="recipes/2/tasks/3/logs/machinedesc.log" filename="machinedesc.log"/>
        <log path="recipes/2/tasks/3/logs/lshw.log" filename="lshw.log"/>
        <log path="recipes/2/tasks/3/logs/df.log" filename="df.log"/>
        <log path="recipes/2/tasks/3/logs/mount.log" filename="mount.log"/>
        <log path="recipes/2/tasks/3/logs/installedpkgs.log" filename="installedpkgs.log"/>
        <log path="recipes/2/tasks/3/logs/test_console.log" filename="test_console.log"/>
      </logs>
      <fetch url="https://server/kernel-tests-main.zip#test/misc/machineinfo"/>
      <params>
        <param name="CKI_ID" value="40"/>
        <param name="CKI_NAME" value="machineinfo"/>
        <param name="CKI_UNIVERSAL_ID" value="machineinfo"/>
        <param name="CKI_WAIVED" value="True"/>
        <param name="JOB_MEMBERS" value="host-abc.redhat.com"/>
        <param name="RECIPE_MEMBERS" value="host-abc.redhat.com"/>
      </params>
      <results>
        <result id="1" path="test/misc/machineinfo" result="PASS">
          <logs>
            <log path="recipes/2/tasks/3/results/1/logs/dmesg.log" filename="dmesg.log"/>
            <log path="recipes/2/tasks/3/results/1/logs/avc.log" filename="avc.log"/>
          </logs>
        </result>
      </results>
    </task>
  </recipe>
</recipeSet>
"""

job_example = """
<job group="cki">
  <recipeSet>
    <recipe id="1" status="Completed" result="PASS" job_id="1">
      <task name="SELinux Custom Module Setup" role="None" id="1" status="Completed" result="None" start_time="2022-05-17T14:55:40+0000" end_time="2022-05-17T14:56:02+0000" duration="22">
        <logs>
          <log path="recipes/1/tasks/1/logs/harness.log" filename="harness.log"/>
          <log path="recipes/1/tasks/1/logs/taskout.log" filename="taskout.log"/>
        </logs>
        <fetch url="https://server/kernel-tests-main.zip#distribution/selinux-custom-modules"/>
        <params>
          <param name="CKI_ID" value="33"/>
          <param name="CKI_NAME" value="SELinux Custom Module Setup"/>
          <param name="CKI_UNIVERSAL_ID" value="selinux_custom_module_setup"/>
          <param name="CKI_WAIVED" value="True"/>
          <param name="None" value="rhost-abc.redhat.com"/>
          <param name="JOB_MEMBERS" value="rhost-abc.redhat.com"/>
          <param name="RECIPE_MEMBERS" value="rhost-abc.redhat.com"/>
        </params>
        <results>
          <result id="1" path="/kernel/distribution/selinux-custom-modules" result="SKIP">
            <logs>
              <log path="recipes/1/tasks/1/results/1/logs/dmesg.log" filename="dmesg.log"/>
            </logs>
          </result>
        </results>
      </task>
      <task name="/distribution/check-install" role="STANDALONE" id="2" status="Completed" result="PASS" start_time="2022-05-17T14:56:03+0000" end_time="2022-05-17T14:56:21+0000" duration="18">
        <logs>
          <log path="recipes/1/tasks/2/logs/harness.log" filename="harness.log"/>
          <log path="recipes/1/tasks/2/logs/taskout.log" filename="taskout.log"/>
          <log path="recipes/1/tasks/2/logs/test_console.log" filename="test_console.log"/>
        </logs>
        <fetch url="https://git-server/archive/master.zip#check-install"/>
        <params>
          <param name="STANDALONE" value="rhost-abc.redhat.com"/>
          <param name="JOB_MEMBERS" value="rhost-abc.redhat.com"/>
          <param name="RECIPE_MEMBERS" value="rhost-abc.redhat.com"/>
        </params>
        <results>
          <result id="1" path="/distribution/check-install" result="PASS">
            <logs>
              <log path="recipes/1/tasks/2/results/1/logs/dmesg.log" filename="dmesg.log"/>
              <log path="recipes/1/tasks/2/results/1/logs/avc.log" filename="avc.log"/>
            </logs>
          </result>
          <result id="2" path="/distribution/check-install/Sysinfo" result="PASS">
            <logs>
              <log path="recipes/1/tasks/2/results/2/logs/avc.log" filename="avc.log"/>
            </logs>
          </result>
        </results>
      </task>
      <task name="/test/misc/machineinfo" id="3" status="Completed" result="PASS" start_time="2022-05-17T14:56:24+0000" end_time="2022-05-17T14:56:44+0000" duration="20">
        <logs>
          <log path="recipes/1/tasks/3/logs/harness.log" filename="harness.log"/>
          <log path="recipes/1/tasks/3/logs/taskout.log" filename="taskout.log"/>
          <log path="recipes/1/tasks/3/logs/machinedesc.log" filename="machinedesc.log"/>
          <log path="recipes/1/tasks/3/logs/lshw.log" filename="lshw.log"/>
          <log path="recipes/1/tasks/3/logs/df.log" filename="df.log"/>
          <log path="recipes/1/tasks/3/logs/mount.log" filename="mount.log"/>
          <log path="recipes/1/tasks/3/logs/installedpkgs.log" filename="installedpkgs.log"/>
          <log path="recipes/1/tasks/3/logs/test_console.log" filename="test_console.log"/>
        </logs>
        <fetch url="https://server/kernel-tests-main.zip#test/misc/machineinfo"/>
        <params>
          <param name="CKI_ID" value="34"/>
          <param name="CKI_NAME" value="machineinfo"/>
          <param name="CKI_UNIVERSAL_ID" value="machineinfo"/>
          <param name="CKI_WAIVED" value="True"/>
          <param name="JOB_MEMBERS" value="rhost-abc.redhat.com"/>
          <param name="RECIPE_MEMBERS" value="rhost-abc.redhat.com"/>
        </params>
        <results>
          <result id="1" path="test/misc/machineinfo" result="PASS">
            <logs>
              <log path="recipes/1/tasks/3/results/1/logs/dmesg.log" filename="dmesg.log"/>
              <log path="recipes/1/tasks/3/results/1/logs/avc.log" filename="avc.log"/>
            </logs>
          </result>
        </results>
      </task>
    </recipe>
    <recipe id="2" status="Completed" result="FAIL" job_id="1">
      <task name="SELinux Custom Module Setup" role="None" id="1" status="Completed" result="None" start_time="2022-05-17T14:55:38+0000" end_time="2022-05-17T14:55:55+0000" duration="17">
        <logs>
          <log path="recipes/2/tasks/1/logs/harness.log" filename="harness.log"/>
          <log path="recipes/2/tasks/1/logs/taskout.log" filename="taskout.log"/>
          <log path="recipes/2/tasks/1/logs/test_console.log" filename="test_console.log"/>
        </logs>
        <fetch url="https://server/kernel-tests-main.zip#distribution/selinux-custom-modules"/>
        <params>
          <param name="CKI_ID" value="39"/>
          <param name="CKI_NAME" value="SELinux Custom Module Setup"/>
          <param name="CKI_UNIVERSAL_ID" value="selinux_custom_module_setup"/>
          <param name="CKI_WAIVED" value="True"/>
          <param name="None" value="rhost-abc.redhat.com"/>
          <param name="JOB_MEMBERS" value="rhost-abc.redhat.com"/>
          <param name="RECIPE_MEMBERS" value="rhost-abc.redhat.com"/>
        </params>
        <results>
          <result id="1" path="/kernel/distribution/selinux-custom-modules" result="SKIP">
            <logs>
              <log path="recipes/2/tasks/1/results/1/logs/dmesg.log" filename="dmesg.log"/>
            </logs>
          </result>
        </results>
      </task>
      <task name="/distribution/check-install" role="STANDALONE" id="2" status="Completed" result="PASS" start_time="2022-05-17T14:55:56+0000" end_time="2022-05-17T14:56:13+0000" duration="17">
        <logs>
          <log path="recipes/2/tasks/2/logs/harness.log" filename="harness.log"/>
          <log path="recipes/2/tasks/2/logs/taskout.log" filename="taskout.log"/>
          <log path="recipes/2/tasks/2/logs/test_console.log" filename="test_console.log"/>
        </logs>
        <fetch url="https://git-server/archive/master.zip#check-install"/>
        <params>
          <param name="STANDALONE" value="rhost-abc.redhat.com"/>
          <param name="JOB_MEMBERS" value="rhost-abc.redhat.com"/>
          <param name="RECIPE_MEMBERS" value="rhost-abc.redhat.com"/>
        </params>
        <results>
          <result id="1" path="/distribution/check-install" result="PASS">
            <logs>
              <log path="recipes/2/tasks/2/results/1/logs/dmesg.log" filename="dmesg.log"/>
              <log path="recipes/2/tasks/2/results/1/logs/avc.log" filename="avc.log"/>
            </logs>
          </result>
          <result id="2" path="/distribution/check-install/Sysinfo" result="PASS">
            <logs>
              <log path="recipes/2/tasks/2/results/2/logs/avc.log" filename="avc.log"/>
            </logs>
          </result>
        </results>
      </task>
      <task name="/test/misc/machineinfo" id="3" status="Completed" result="PASS" start_time="2022-05-17T14:56:17+0000" end_time="2022-05-17T14:56:36+0000" duration="19">
        <logs>
          <log path="recipes/2/tasks/3/logs/harness.log" filename="harness.log"/>
          <log path="recipes/2/tasks/3/logs/taskout.log" filename="taskout.log"/>
          <log path="recipes/2/tasks/3/logs/machinedesc.log" filename="machinedesc.log"/>
          <log path="recipes/2/tasks/3/logs/lshw.log" filename="lshw.log"/>
          <log path="recipes/2/tasks/3/logs/df.log" filename="df.log"/>
          <log path="recipes/2/tasks/3/logs/mount.log" filename="mount.log"/>
          <log path="recipes/2/tasks/3/logs/installedpkgs.log" filename="installedpkgs.log"/>
          <log path="recipes/2/tasks/3/logs/test_console.log" filename="test_console.log"/>
        </logs>
        <fetch url="https://server/kernel-tests-main.zip#test/misc/machineinfo"/>
        <params>
          <param name="CKI_ID" value="40"/>
          <param name="CKI_NAME" value="machineinfo"/>
          <param name="CKI_UNIVERSAL_ID" value="machineinfo"/>
          <param name="CKI_WAIVED" value="True"/>
          <param name="JOB_MEMBERS" value="rhost-abc.redhat.com"/>
          <param name="RECIPE_MEMBERS" value="rhost-abc.redhat.com"/>
        </params>
        <results>
          <result id="1" path="test/misc/machineinfo" result="PASS">
            <logs>
              <log path="recipes/2/tasks/3/results/1/logs/dmesg.log" filename="dmesg.log"/>
              <log path="recipes/2/tasks/3/results/1/logs/avc.log" filename="avc.log"/>
            </logs>
          </result>
        </results>
      </task>
    </recipe>
  </recipeSet>
</job>
"""
