"""Test for upt.smoke_test.render module."""
from io import StringIO
from pathlib import Path
import tempfile
import unittest
from unittest.mock import patch
import xml.etree.ElementTree as ET

from tests.const import ASSETS_DIR
from upt.smoke_test import render


class TestRender(unittest.TestCase):
    """Test template rendering."""

    maxDiff = None

    def check_recipesets(self, xml_content: str, number_of_recipesets: int) -> None:
        """Helper method."""
        tree = ET.fromstring(xml_content)
        self.assertEqual(number_of_recipesets, len(tree.findall('.//recipeSet')))

    def setUp(self):
        """Common stuff."""
        self.template_file = 'smoke_test.j2'
        self.all_smoke_tests = {
            'smoke_test': render.SMOKE_TESTS,
            'arch': 'x86_64',
            'distro': 'CentOSStream9',
        }

        self.warn_smoke_test = {
            'smoke_test': ['warn'],
            'arch': 'x86_64',
            'distro': 'CentOSStream9',
        }

    def test_get_template_variables(self):
        """Test get template variables."""
        cases = (
            {
                'description': 'With default values',
                'parsed_vars': {
                    'arch': 'x86_64',
                    'distro': 'CentOSStream9',
                    'smoke_test': []
                },
                'expected': {
                    'arch': 'x86_64',
                    'distro': 'CentOSStream9',
                    'smoke_test': render.SMOKE_TESTS
                }
            },
            {
                'description': 'With custom values',
                'parsed_vars': {
                    'arch': 'aarch64',
                    'distro': 'RedHatEnterpriseLinux7',
                    'smoke_test': [['abort_recipe'], ['fail']]
                },
                'expected': {
                    'arch': 'aarch64',
                    'distro': 'RedHatEnterpriseLinux7',
                    'smoke_test': ['abort_recipe', 'fail']
                }
            },
            {
                'description': 'With smoke_test elements repeated',
                'parsed_vars': {
                    'arch': 'aarch64',
                    'distro': 'RedHatEnterpriseLinux7',
                    'smoke_test': [['abort_recipe'], ['fail'], ['fail']]
                },
                'expected': {
                    'arch': 'aarch64',
                    'distro': 'RedHatEnterpriseLinux7',
                    'smoke_test': ['abort_recipe', 'fail']
                }
            },
        )
        for case in cases:
            with self.subTest(case['description']):
                args = render.get_template_variables(case['parsed_vars'])
                self.assertEqual(args['arch'], case['expected']['arch'])
                self.assertEqual(args['distro'], case['expected']['distro'])
                self.assertCountEqual(args['smoke_test'], case['expected']['smoke_test'])

    def test_render_function(self):
        """Test render function."""

        cases = (
            {
                'description': 'All smoke_test',
                'template_file': self.template_file,
                'template_variables': self.all_smoke_tests,
            },
            {
                'description': 'One smoke_test',
                'template_file': self.template_file,
                'template_variables': self.warn_smoke_test,
            }

        )

        for case in cases:
            with self.subTest(case['description']):
                output = render.render(case['template_file'], case['template_variables'])
                self.check_recipesets(output, len(case['template_variables']['smoke_test']))

    def test_main_using_stdout(self):
        """Test main function."""
        cases = (
            {
                'description': 'With default values',
                'args': [],
                'expected': len(render.SMOKE_TESTS)
            },
            {
                'description': 'Selecting smoke tests',
                'args': ['--smoke-test', 'warn'],
                'expected': 1
            }
        )
        for case in cases:
            with self.subTest(case['description']):
                with patch('sys.stdout', new=StringIO()) as output:
                    render.main(case['args'])
                    self.check_recipesets(output.getvalue(), case['expected'])

    def test_main_using_files(self):
        """Test main function."""
        output_file = 'beaker.xml'
        cases = (
            {
                'description': 'With default values',
                'args': ['--output', output_file],
                'expected': len(render.SMOKE_TESTS)
            },
            {
                'description': 'Selecting smoke tests',
                'args': ['--smoke-test', 'warn', '--output', output_file],
                'expected': 1
            }
        )
        for case in cases:
            with self.subTest(case['description']):
                with tempfile.TemporaryDirectory():
                    render.main(case['args'])
                    xml_content = Path(output_file).read_text(encoding='utf-8')
                    self.check_recipesets(xml_content, case['expected'])

    def test_check_output(self):
        """Check output."""
        assest_content = Path(ASSETS_DIR, 'beaker_smoke_test.xml').read_text(encoding='utf-8')
        xml_content = render.render(self.template_file, self.all_smoke_tests)
        self.assertEqual(assest_content, xml_content)
        self.assertEqual(ET.canonicalize(assest_content), ET.canonicalize(xml_content))
